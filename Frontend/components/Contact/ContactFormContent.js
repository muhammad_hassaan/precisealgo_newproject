import React, { useEffect, useState } from "react";
import ContactForm from "./ContactForm";

const ContactFormContent = (props) => {
  // console.log(props.data[1].social_section);

  const socialItem = props.data[1]?.social_section;

  return (
    <>
      <section className="contact-area ptb-110">
        <div className="container">
          {props.data?.map((item) => {
            if (item.type === "offer_solution_item") {
              return (
                <div className="section-title">
                  <span>{item.icon}</span>
                  <h2>{item.title}</h2>
                  <p>{item.description}</p>
                </div>
              );
            }
          })}

          <div className="contact-form">
            <div className="row align-items-center">
              <div className="col-lg-5 col-md-12">
                <div className="contact-image">
                  <img src="/images/contact.png" alt="image" />
                </div>
              </div>

              <div className="col-lg-7 col-md-12">
                <ContactForm />
              </div>
            </div>
          </div>

          {/* Contact info */}
          <div className="contact-info">
            <div className="contact-info-content">
              {props.data?.map((item) => {
                if (item.type === "contact_section") {
                  return (
                    <div>
                      <h3>{item.title}</h3>
                      <h2>
                        <span className="number">{item.contact_no}</span>
                        <span className="or">OR</span>
                        <span className="email">{item.email}</span>
                      </h2>
                    </div>
                  );
                }
              })}

              <ul className="social">
                {socialItem?.map((item) => {
                  {/* console.log(item); */}
                  return (
                    <li>
                      <a
                        href={item.link}
                        target="_blank"
                        rel="noreferrer"
                      >
                        <i className={item.icon_classnames}></i>
                      </a>
                    </li>
                  );
                })}

                {/* <li>
                  <a
                    href="https://www.youtube.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="fab fa-youtube"></i>
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.facebook.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="fab fa-facebook-f"></i>
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.linkedin.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="fab fa-linkedin-in"></i>
                  </a>
                </li>
                <li>
                  <a
                    href="https://www.instagram.com/"
                    target="_blank"
                    rel="noreferrer"
                  >
                    <i className="fab fa-instagram"></i>
                  </a>
                </li> */}
              </ul>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};
export default ContactFormContent;
