import React, { useEffect, useState } from "react";
import FsLightbox from "fslightbox-react";
import Link from "next/link";

const Webinar = (props) => {
  // console.log('webinar', props.data.image)
  const [toggler, setToggler] = useState(false);
  const[webinarImage, setWebinarImage] = useState('');

  useEffect(() => {
      setWebinarImage(props.data.image);
  }, [props.data.image]);

  return (
    <>
      <FsLightbox
        toggler={toggler}
        sources={["https://www.youtube.com/embed/bk7McNUjWgw"]}
      />

      <section className="webinar-area">
        <div className="row m-0">
          <div className="col-lg-6 p-0">
            <div className="webinar-content">
              <h2>{props.data.title}</h2>
              <p>
              {props.data.description}
              </p>

              <Link href="#" className="btn btn-primary">
                Watch More
              </Link>
            </div>
          </div>

          <div className="col-lg-6 p-0">
            <div className="webinar-video-image">
              <img src={webinarImage} alt="image" />

              <div
                onClick={() => setToggler(!toggler)}
                className="video-btn popup-youtube"
                data-aos="zoom-in"
                data-aos-duration="1200"
                data-aos-delay="600"
              >
                <i className="flaticon-play-button"></i>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Webinar;
