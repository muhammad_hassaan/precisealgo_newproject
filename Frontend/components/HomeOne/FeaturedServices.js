import React from "react";
import Link from "next/link";

const FeaturedServices = (props) => {

  const featuredServicesData = props.data;

  return (
    <>
      <div className="featured-services-area">
        <div className="container">
          <div className="row justify-content-center">
            {featuredServicesData.map((item)=> (
            <div 
              className="col-lg-4 col-md-6 col-sm-6"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="200"
            >
              <div className="single-featured-services-box">
                <div className="icon">
                {/* {ReactHtmlParser(item.icon)} */}
                <div dangerouslySetInnerHTML={{ __html: item.icon }} />
                </div>

                <h3>
                  <Link href="/service-details">
                    {item.title}
                  </Link>
                </h3>
                <p>
                  {item.description}
                </p>
              </div>
            </div>))}

            <div 
              className="col-lg-4 col-md-6 col-sm-6"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="400"
            >
              {/* <div className="single-featured-services-box active">
                <div className="icon">
                  <i className="flaticon-artificial-intelligence"></i>
                </div>

                <h3>
                  <Link href="/service-details">Cognitive Automation</Link>
                </h3>
                <p>
                  Lorem ipsum dolor consectetur adipiscing elit, sed do eiusmod
                  tempor incididunt ut labore. Ut enim ad minim veniam.
                </p>
              </div> */}
            </div>

            <div 
              className="col-lg-4 col-md-6 col-sm-6"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="600"
            >
              {/* <div className="single-featured-services-box">
                <div className="icon">
                  <i className="flaticon-machine-learning"></i>
                </div>

                <h3>
                  <Link href="/service-details">Cognitive Engagement</Link>
                </h3>
                <p>
                  Lorem ipsum dolor consectetur adipiscing elit, sed do eiusmod
                  tempor incididunt ut labore. Ut enim ad minim veniam.
                </p>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default FeaturedServices;
