import React, { useEffect, useState } from "react";
import Link from "next/link";
import FeaturedServices from "./FeaturedServices";


const MainBanner = (props) => {

  const[bannerImageUrl, setBannerImageUrl] = useState('');
  
  useEffect(() => {
    setBannerImageUrl(props.data.image);
  }, [props.data.image]);

  const bannerStyle = {
    backgroundImage: `url(${bannerImageUrl})`
  };
  return (
    <>
      <div className="main-banner" style={bannerStyle}>
        <div className="d-table">
          <div className="d-table-cell">
            <div className="container-fluid">
              <div className="main-banner-content">
                <h1
                  data-aos="fade-up"
                  data-aos-duration="1200"
                  data-aos-delay="100"
                >
                 {props.data.title}
                </h1>

                <p
                  data-aos="fade-up"
                  data-aos-duration="1200"
                  data-aos-delay="200"
                >
                {props.data.description}
                </p>

                <div
                  className="btn-box"
                  data-aos="fade-up"
                  data-aos-duration="1200"
                  data-aos-delay="300"
                >
                  <Link href="/contact" className="btn btn-primary">
                    Schedule A Demo
                  </Link>

                  <Link href="/contact" className="optional-btn">
                    Get Started Free
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Featured Services */}
        <FeaturedServices data={props.data.hero_service} />
      </div>
    </>
  );
};

export default MainBanner;
