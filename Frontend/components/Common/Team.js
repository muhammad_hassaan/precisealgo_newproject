import React, { useEffect, useState } from "react";
import axios from "axios";

const Team = () => {
  const clientID = "test_client";
  const secretKey = "asdf1234";
  const userName = "api";
  const accessTokenURL = "http://precise-algo.local.com/oauth/token";

  const [paragraphs, setParagraphs] = useState([]);
  // const [breadCrumbs, setbreadCrumbs] = useState();
  const [teamContent, setTeamContent] = useState();
  const [socialIcon, setSocialIcon] = useState();
  const [imgUrls, setImgUrls] = useState();

  // const paragraphs = data;
  useEffect(() => {
    const access_token = localStorage.getItem("token");
    if (access_token == null) {
      getToken();
    } else {
      fetchPageData(access_token);
    }
  }, []);

  // useEffect(() => {
  //   if (paragraphs !== undefined) {
  //     // console.log('in useEffect', paragraphs['paragraphs']);
  //     const arr = paragraphs["paragraphs"];
  //     arr?.map((p) => {
  //       const { type } = p;
  //       if (type === "about_us_team") {
  //         setTeamContent(p);
  //   }});
  //   }
  // }, [paragraphs]);

  const getToken = async () => {
    const data = {
      grant_type: "password",
      username: userName,
      password: secretKey,
      client_id: clientID,
    };
    const options = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      withCredentials: true,
    };
    await axios.post(accessTokenURL, data, options).then((response) => {
      // console.log(response.data.access_token);
      fetchPageData(response.data.access_token);
      // fetchPageData2(response.data.access_token);
      localStorage.setItem("token", response.data.access_token);
      return true;
    });
  };
  const fetchPageData = async (access_token) => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access_token}`,
    };
    await axios
      .get("http://precise-algo.local.com/fetch/about", { headers })
      .then((response) => {
        setParagraphs(response.data.paragraphs[2]);
        setTeamContent(response.data.paragraphs[2]?.paragraphs);
        setSocialIcon(response.data.paragraphs[2].paragraphs[0].social);
        // console.log(response.data.paragraphs[2]?.paragraphs);
        // console.log('team-section-about',response.data.paragraphs[2].paragraphs.imgUrl);
        const imageUrls = response.data.paragraphs[2].paragraphs.map(
          (item) => item.imgUrl
        );
        setImgUrls(imageUrls);
      });
  };
  console.log(teamContent);

  return (
    <>
      <section className="team-area ptb-110">
        <div className="container">
          <div className="section-title">
            <h2>{paragraphs.title}</h2>
            <p>{paragraphs.description}</p>
          </div>

          <div className="row justify-content-center">
            {teamContent?.map((item) => {
              return (
                <div
                  className="col-lg-3 col-sm-6"
                  data-aos="fade-in"
                  data-aos-duration="1200"
                  data-aos-delay="200"
                >
                  <div className="single-team-box">
                    <div className="image">
                      <img src={item.imgUrl} alt="Team Image" />

                      <div className="social">
                        {socialIcon.map((item) => {
                          return (
                            <a
                              href={item.title}
                              target="_blank"
                              rel="noreferrer"
                            >
                              <i className={item.icon}></i>
                            </a>
                          );
                        })}

                        {/* <a
                          href="https://twitter.com/"
                          target="_blank"
                          rel="noreferrer"
                        >
                          <i className="fab fa-twitter"></i>
                        </a>

                        <a
                          href="https://www.instagram.com/"
                          target="_blank"
                          rel="noreferrer"
                        >
                          <i className="fab fa-instagram"></i>
                        </a> */}
                      </div>
                    </div>

                    <div className="content">
                      <h3>{item.title}</h3>
                      <span>{item.designation}</span>
                    </div>
                  </div>
                </div>
              );
            })}

            {/* <div
              className="col-lg-3 col-sm-6"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="300"
            >
              <div className="single-team-box">
                <div className="image">
                  <img src="/images/team/team2.jpg" alt="Team Image" />

                  <div className="social">
                    <a
                      href="https://www.facebook.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fab fa-facebook-f"></i>
                    </a>

                    <a
                      href="https://twitter.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fab fa-twitter"></i>
                    </a>

                    <a
                      href="https://www.instagram.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fab fa-instagram"></i>
                    </a>
                  </div>
                </div>

                <div className="content">
                  <h3>Lucy Eva</h3>
                  <span>React Developer</span>
                </div>
              </div>
            </div>

            <div
              className="col-lg-3 col-sm-6"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="400"
            >
              <div className="single-team-box">
                <div className="image">
                  <img src="/images/team/team3.jpg" alt="Team Image" />

                  <div className="social">
                    <a
                      href="https://www.facebook.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fab fa-facebook-f"></i>
                    </a>

                    <a
                      href="https://twitter.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fab fa-twitter"></i>
                    </a>

                    <a
                      href="https://www.instagram.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fab fa-instagram"></i>
                    </a>
                  </div>
                </div>

                <div className="content">
                  <h3>Steven Smith</h3>
                  <span>Web Developer</span>
                </div>
              </div>
            </div>

            <div
              className="col-lg-3 col-sm-6"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="500"
            >
              <div className="single-team-box">
                <div className="image">
                  <img src="/images/team/team4.jpg" alt="Team Image" />

                  <div className="social">
                    <a
                      href="https://www.facebook.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fab fa-facebook-f"></i>
                    </a>

                    <a
                      href="https://twitter.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fab fa-twitter"></i>
                    </a>

                    <a
                      href="https://www.instagram.com/"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <i className="fab fa-instagram"></i>
                    </a>
                  </div>
                </div>

                <div className="content">
                  <h3>Sarah Taylor</h3>
                  <span>Designer</span>
                </div>
              </div>
            </div> */}
          </div>
        </div>
      </section>
    </>
  );
};

export default Team;
