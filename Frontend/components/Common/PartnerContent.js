import React, { useEffect, useState } from "react";

const PartnerContent = (props) => {
  // console.log("par", props.data.paragraphs[0].imgUrl);

  const [imgUrl, setImgUrl] = useState([]);

  useEffect(() => {
    setImgUrl(props.data.paragraphs);
  }, [props.data.paragraphs]);

  return (
    <>
      <div className="partner-area ptb-110 bg-f2f6f9">
        <div className="container">
          <div className="section-title">
            <h2>{props.data.title}</h2>
            <p>{props.data.description}</p>
          </div>

          <div className="customers-partner-list">
            {imgUrl.map((item) => {
              return (
                <div
                  className="partner-item"
                  data-aos="fade-in"
                  data-aos-duration="1200"
                  data-aos-delay="100"
                >
                  <a href="#" target="_blank" rel="noreferrer">
                    <img src={item.imgUrl} alt="image" />
                  </a>
                </div>
              );
            })}
            {/* <div
              className="partner-item"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="100"
            >
              <a href="#" target="_blank" rel="noreferrer">
                <img src="/images/partner/partner1.png" alt="image" />
              </a>
            </div> */}

            {/* <div 
              className="partner-item"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="200"
            >
              <a href="#" target="_blank" rel="noreferrer">
                <img src="/images/partner/partner2.png" alt="image" />
              </a>
            </div>

            <div 
              className="partner-item"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="300"
            >
              <a href="#" target="_blank" rel="noreferrer">
                <img src="/images/partner/partner3.png" alt="image" />
              </a>
            </div>

            <div 
              className="partner-item"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="400"
            >
              <a href="#" target="_blank" rel="noreferrer">
                <img src="/images/partner/partner4.png" alt="image" />
              </a>
            </div>

            <div 
              className="partner-item"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="500"
            >
              <a href="#" target="_blank" rel="noreferrer">
                <img src="/images/partner/partner5.png" alt="image" />
              </a>
            </div>

            <div 
              className="partner-item"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="600"
            >
              <a href="#" target="_blank" rel="noreferrer">
                <img src="/images/partner/partner6.png" alt="image" />
              </a>
            </div>

            <div 
              className="partner-item"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="700"
            >
              <a href="#" target="_blank" rel="noreferrer">
                <img src="/images/partner/partner1.png" alt="image" />
              </a>
            </div>

            <div 
              className="partner-item"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="800"
            >
              <a href="#" target="_blank" rel="noreferrer">
                <img src="/images/partner/partner2.png" alt="image" />
              </a>
            </div>

            <div 
              className="partner-item"
              data-aos="fade-in"
              data-aos-duration="1200"
              data-aos-delay="900"
            >
              <a href="#" target="_blank" rel="noreferrer">
                <img src="/images/partner/partner3.png" alt="image" />
              </a>
            </div> */}
          </div>
        </div>
      </div>
    </>
  );
};

export default PartnerContent;
