import React, { Component, useEffect, useState } from "react";
import Link from "next/link";
import axios from "axios";

const Footer = () => {
  const clientID = "test_client";
  const secretKey = "asdf1234";
  const userName = "api";
  const accessTokenURL = "http://precise-algo.local.com/oauth/token";

  const [paragraphs, setParagraphs] = useState([]);
  const [menuLinks, setmenuLinks] = useState();
  const [serviceLinks, setserviceLinks] = useState();
  const [contactDetail, setContactDetail] = useState();

  useEffect(() => {
    const access_token = localStorage.getItem("token");
    const token_expiry = localStorage.getItem("expiry");

    if (access_token == null || Date.now() > token_expiry) {
      getToken();
    } else if (Date.now() <= token_expiry - 5 * 60 * 1000) {
      getToken();
    } else {
      fetchPageData(access_token);
    }
  }, []);

  useEffect(() => {
    if (paragraphs !== undefined) {
      const arr = paragraphs["paragraphs"];
      arr?.map((p) => {
        const { type } = p;
        if (type === "carousel_wraper") {
          setCarousel(p);
        } else if (type === "offer_solution") {
          setService(p);
        }
      });
    }
  }, [paragraphs]);

  const getToken = async () => {
    const data = {
      grant_type: "password",
      username: userName,
      password: secretKey,
      client_id: clientID,
    };
    const options = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      withCredentials: true,
    };
    await axios.post(accessTokenURL, data, options).then((response) => {
      fetchPageData(response.data.access_token);
      localStorage.setItem("token", response.data.access_token);
      return true;
    });
  };
  const fetchPageData = async (access_token) => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access_token}`,
    };

    await axios
      .get("http://precise-algo.local.com/fetch/footer", { headers })
      .then((response) => {
        setParagraphs(response.data);
        setContactDetail(response.data.contact);
        setmenuLinks(response.data.menu_items);
        setserviceLinks(response.data.services);
      });
  };

  const currentYear = new Date().getFullYear();
  return (
    <>
      <footer className="footer-area">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div
                className="single-footer-widget"
                data-aos="fade-in"
                data-aos-duration="1200"
                data-aos-delay="100"
              >
                <div className="logo">
                  <Link href="/">
                    <img src="/images/white-logo.png" alt="image" />
                  </Link>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud.
                  </p>
                </div>

                <ul className="social">
                  <li>
                    <a href="https://www.facebook.com/" target="_blank">
                      <i className="flaticon-facebook-letter-logo"></i>
                    </a>
                  </li>
                  <li>
                    <a href="https://twitter.com/" target="_blank">
                      <i className="flaticon-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="https://www.instagram.com/" target="_blank">
                      <i className="flaticon-instagram-logo"></i>
                    </a>
                  </li>
                  <li>
                    <a href="https://www.youtube.com/" target="_blank">
                      <i className="flaticon-youtube-play-button"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>

            <div className="col-lg-3 col-md-6 col-sm-6">
              <div
                className="single-footer-widget"
                data-aos="fade-in"
                data-aos-duration="1200"
                data-aos-delay="200"
              >
                <h3>Services</h3>

                <ul className="footer-services-list">
                  {serviceLinks?.map((item) => {
                    return (
                      <li>
                        <Link href="/service-details">{item.title}</Link>
                      </li>
                    );
                  })}

                  {/* <li>
                    <Link href="/service-details">UX/UI Design</Link>
                  </li>
                  <li>
                    <Link href="/service-details">Big Data Analysis</Link>
                  </li>
                  <li>
                    <Link href="/service-details">Desktop Applications</Link>
                  </li>
                  <li>
                    <Link href="/service-details">Mobile Applications</Link>
                  </li> */}
                </ul>
              </div>
            </div>

            <div className="col-lg-3 col-md-6 col-sm-6">
              <div
                className="single-footer-widget"
                data-aos="fade-in"
                data-aos-duration="1200"
                data-aos-delay="300"
              >
                <h3>Quick Links</h3>
                <ul className="quick-links-list">
                  {menuLinks?.map((item) => {
                    return (
                      <li>
                        <Link href="/about">{item.title}</Link>
                      </li>
                    );
                  })}

                  {/* <li>
                    <Link href="/blog">Blog</Link>
                  </li>
                  <li>
                    <Link href="/contact">Contact</Link>
                  </li>
                  <li>
                    <Link href="/services">Services</Link>
                  </li>
                  <li>
                    <Link href="/pricing">Pricing</Link>
                  </li> */}
                </ul>
              </div>
            </div>

            <div className="col-lg-3 col-md-6 col-sm-6">
              <div
                className="single-footer-widget"
                data-aos="fade-in"
                data-aos-duration="1200"
                data-aos-delay="400"
              >
                <h3>Contacts</h3>

                <ul className="footer-contact-list">
                  <li>
                    <span>Address:</span>
                    {/* 2750 Quadra Street <br /> Victoria, Canada */}
                    {contactDetail?.address}
                  </li>
                  <li>
                    <span>Email:</span>
                    {contactDetail?.email}
                  </li>
                  <li>
                    <span>Phone:</span>
                    {contactDetail?.phone}
                  </li>
                  <li>
                    <span>Fax:</span>
                    {contactDetail?.fax}
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div className="copyright-area">
          <div className="container">
            <div className="row align-items-center">
              <div className="col-lg-6 col-md-6 col-sm-6">
                <p>
                  Copyright &copy;{currentYear} Bariton. All Rights Reserved{" "}
                  <a href="https://envytheme.com/" target="_blank">
                    EnvyTheme
                  </a>
                  .
                </p>
              </div>

              <div className="col-lg-6 col-md-6 col-sm-6">
                <ul>
                  <li>
                    <Link href="/privacy-policy">Privacy Policy</Link>
                  </li>
                  <li>
                    <Link href="/terms-conditions">Terms & Conditions</Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div className="circle-map">
          <img src="/images/circle-map.png" alt="image" />
        </div>

        <div className="lines">
          <div className="line"></div>
          <div className="line"></div>
          <div className="line"></div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
