import React from "react";

const ProjectsDetailsContent = (props) => {
  // console.log(props.data);
  return (
    <>
      <div className="projects-details-area ptb-110">
        <div className="container">
          <div className="projects-details">
            <div className="row">
              <div className="col-lg-4 col-md-6 col-sm-6">
                <div className="projects-details-image">
                  <img src="/images/projects-img1.jpg" alt="image" />
                </div>
              </div>

              <div className="col-lg-4 col-md-6 col-sm-6">
                <div className="projects-details-image">
                  <img src="/images/projects-img2.jpg" alt="image" />
                </div>
              </div>

              <div className="col-lg-4 col-md-12 col-sm-12">
                <div className="projects-details-info">
                  <div className="d-table">
                    <div className="d-table-cell">
                      <ul>
                        <li>
                          <span>Customer Name:</span> {props.data.customername}
                        </li>
                        <li>
                          <span>Category:</span> Design, Idea
                        </li>
                        <li>
                          <span>Start Date:</span> {props.data.startdate}
                        </li>
                        <li>
                          <span>End Date:</span> {props.data.enddate}
                        </li>
                        <li>
                          <span>Status:</span> {props.data.status}
                        </li>
                        <li>
                          <span>Demo Link:</span>{" "}
                          <a href="#">{props.data.demolink}</a>
                        </li>
                        <li>
                          <span>Tags:</span> App, Design, Web, Dev
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="projects-details-desc">
              <h3>{props.data.title}</h3>

              <p>{props.data.description}</p>

              <p>{props.data.descriptiontwo}</p>

              <blockquote className="wp-block-quote">
                <p className="mb-0">{props.data.testimonial}</p>
              </blockquote>

              <p>{props.data.descriptionthree}</p>

              <p>{props.data.descriptionfour}</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProjectsDetailsContent;
