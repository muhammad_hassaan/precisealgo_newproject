import React, { useEffect, useState } from "react";
import axios from "axios";


const AboutContent = () => {
  const [subParagraph, setsubParagraph] = useState();
  const[aboutImage1, setAboutImage1] = useState('');
  const[aboutImage2, setAboutImage2] = useState('');


  // require('dotenv').config()
  const clientID = "test_client";
  const secretKey = "asdf1234";
  const userName = "api";
  const accessTokenURL = "http://precise-algo.local.com/oauth/token";

  const [paragraphs, setParagraphs] = useState([]);

  // const apiURL = process.env.REACT_APP_ABOUT_PAGE_API_URL;
  // console.log(apiURL);

  useEffect(() => {
    getToken();
  }, []);

  const getToken = async () => {
    const data = {
      grant_type: "password",
      username: userName,
      password: secretKey,
      client_id: clientID,
    };
    const options = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      withCredentials: true,
    };
    await axios.post(accessTokenURL, data, options).then((response) => {
      // console.log(response.data.access_token);
      fetchPageData(response.data.access_token);
      localStorage.setItem("token", response.data.access_token);
      return true;
    });
  };
  const fetchPageData = async (access_token) => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access_token}`,
    };
    await axios
      .get("http://precise-algo.local.com/fetch/about", { headers })
      .then((response) => {
        // console.log("test", response.data.pageTitle);

        setParagraphs(response.data.paragraphs);
        // console.log('data',response.data.paragraphs[0].image1);
        setAboutImage1(response.data.paragraphs[0].image1);
        setAboutImage2(response.data.paragraphs[0].image2)
        setsubParagraph(response.data.paragraphs[1].paragraphs);
      });
  };
  // useEffect(() => {
  //   setAboutImage1(props.data.image1);
  //   setAboutImage2(props.data.image2);
  // }, [props.data]);

  return (
    <>
      <section className="about-area ptb-110">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12">
              <div className="about-image">
                <img src={aboutImage1} alt="image" />
                <img src={aboutImage2} alt="image" />
              </div>
            </div>

            <div className="col-lg-6 col-md-12">
              {/* {console.log(paragraphs[1])} */}
                  <div className="about-content">
                    <h2>{paragraphs[0]?.title}</h2>
                    <p>{paragraphs[0]?.description}</p>
                    <p>{paragraphs[0]?.description}</p>
                  </div>
            </div>
          </div>

          <div className="about-inner-area">
            <div className="row">
              {subParagraph?.map((paragraph) => {
                return (
                  <div className="col-lg-4 col-md-6 col-sm-6">
                    <div className="about-text" key={paragraph.id}>
                      <h3>{paragraph.title}</h3>
                      <p>{paragraph.description}</p>
                    </div>
                  </div>
                );
              })}
              {/* <div className="col-lg-4 col-md-6 col-sm-6">
                <div className="about-text">
                  <h3>Our Mission</h3>
                  <p>
                    Lorem ipsum dolor sit amet, con se ctetur adipiscing elit.
                    In sagittis eg esta ante, sed viverra nunc tinci dunt nec
                    elei fend et tiram.
                  </p>
                </div>
              </div>

              <div className="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                <div className="about-text">
                  <h3>Who we are</h3>
                  <p>
                    Lorem ipsum dolor sit amet, con se ctetur adipiscing elit.
                    In sagittis eg esta ante, sed viverra nunc tinci dunt nec
                    elei fend et tiram.
                  </p>
                </div>
              </div> */}
            </div>
          </div>
        </div>

        {/* Shape Images */}
        <div className="shape-img1">
          <img src="/images/shape/shape1.png" alt="image" />
        </div>
        <div className="shape-img2">
          <img src="/images/shape/shape2.svg" alt="image" />
        </div>
        <div className="shape-img3">
          <img src="/images/shape/shape3.png" alt="image" />
        </div>
        <div className="shape-img4">
          <img src="/images/shape/shape4.svg" alt="image" />
        </div>
        <div className="shape-img5">
          <img src="/images/shape/shape5.svg" alt="image" />
        </div>
        <div className="shape-img6">
          <img src="/images/shape/shape6.png" alt="image" />
        </div>
        <div className="dot-shape1">
          <img src="/images/shape/dot1.png" alt="image" />
        </div>
        <div className="dot-shape2">
          <img src="/images/shape/dot2.png" alt="image" />
        </div>
      </section>
    </>
  );
};

export default AboutContent;
