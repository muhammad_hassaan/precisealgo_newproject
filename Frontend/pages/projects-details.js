import React, { useEffect, useState } from "react";
import NavbarTwo from "../components/Layouts/NavbarTwo";
import PageBanner from "../components/Common/PageBanner";
import Footer from "../components/Layouts/Footer";
import ProjectsDetailsContent from "../components/Projects/ProjectsDetailsContent";
import axios from "axios";
import ScaleLoader from "react-spinners/ScaleLoader";

const ProjectsDetails = () => {
  const clientID = "test_client";
  const secretKey = "asdf1234";
  const userName = "api";
  const accessTokenURL = "http://precise-algo.local.com/oauth/token";

  const [content, setContent] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const access_token = localStorage.getItem("token");
    const token_expiry = localStorage.getItem("expiry");

    if (access_token == null || Date.now() > token_expiry) {
      getToken();
    } else if (Date.now() <= token_expiry - 5 * 60 * 1000) {
      // Token life is less than 5 minutes
      fetchPageData(access_token);
    } else {
      // Use the old token
      fetchPageData();
    }
  }, []);

  const getToken = async () => {
    console.log(URL);
    const data = {
      grant_type: "password",
      username: userName,
      password: secretKey,
      client_id: clientID,
    };
    const options = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      withCredentials: true,
    };
    await axios.post(accessTokenURL, data, options).then((response) => {
      fetchPageData(response.data.access_token);
      localStorage.setItem("token", response.data.access_token);
      return true;
    });
  };
  const fetchPageData = async () => {
    const access_token = localStorage.getItem("token");
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access_token}`,
    };
    await axios
      .get(
        "http://precise-algo.local.com/fetch/project/Incredible-Infrastructure",
        { headers }
      )
      .then((response) => {
        setContent(response.data);
        // console.log(response.data);
        setIsLoading(false);
      });
  };
  return (
    <>
      <NavbarTwo />

      <PageBanner
        pageTitle="Projects Details"
        homePageUrl="/"
        homePageText="Home"
        activePageText="Projects Details"
        bgImgClass="item-bg2"
      />
      {isLoading ? (
        <div className="overlay">
          <div className="spinner">
            <ScaleLoader color={"#ff4800"} loading={isLoading} size={150} />
          </div>
        </div>
      ) : (
        <>
          <ProjectsDetailsContent data={content} />
        </>
      )}
      <Footer />
    </>
  );
};

export default ProjectsDetails;
