import React, { Component, useEffect, useState } from "react";
import NavbarTwo from "../components/Layouts/NavbarTwo";
import PageBanner from "../components/Common/PageBanner";
import AboutContent from "../components/About/AboutContent";
import Services from "../components/HomeOne/Services";
import Team from "../components/Common/Team";
import PartnerContent from "../components/Common/PartnerContent";
import FeedbackSlider from "../components/Common/FeedbackSlider";
import PricingCard from "../components/Common/PricingCard";
import FreeTrialForm from "../components/Common/FreeTrialForm";
import Footer from "../components/Layouts/Footer";
import axios from "axios";
import TeamContent from "../components/Team/TeamContent";
import ScaleLoader from "react-spinners/ScaleLoader";


const About = () => {
  const clientID = "test_client";
  const secretKey = "asdf1234";
  const userName = "api";
  const accessTokenURL = "http://precise-algo.local.com/oauth/token";

  const [paragraphs, setParagraphs] = useState([]);
  // const [breadCrumbs, setbreadCrumbs] = useState();
  const [service, setService] = useState();
  const [partner, setPartner] = useState();
  const [carousel, setCarousel] = useState();
  const [pricing, setPricing] = useState();
  const [breadCrumb, setbreadCrumb] = useState();
  const [isLoading, setIsLoading] = useState(true);

  const URL = process.env["REACT_APP_ABOUT_PAGE_API_URL"];
  console.log('API_URL:', URL);
  // const paragraphs = data;
  useEffect(() => {
    const access_token = localStorage.getItem("token");
    if (access_token == null) {
      getToken();
    } else {
      fetchPageData(access_token);
    }
  }, []);

  useEffect(() => {
    if (paragraphs !== undefined) {
      // console.log('in useEffect', paragraphs['paragraphs']);
      const arr = paragraphs["paragraphs"];
      arr?.map((p) => {
        const { type } = p;
        if (type === "offer_solution") {
          setService(p);
        } else if (type === "customer_partner") {
          setPartner(p);
        } else if (type === "carousel_wraper") {
          setCarousel(p);
        } else if (type === "pricing") {
          setPricing(p);
        }
      });
    }
  }, [paragraphs]);

  const getToken = async () => {
    const data = {
      grant_type: "password",
      username: userName,
      password: secretKey,
      client_id: clientID,
    };
    const options = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      withCredentials: true,
    };
    await axios.post(accessTokenURL, data, options).then((response) => {
      // console.log(response.data.access_token);
      fetchPageData(response.data.access_token);
      // fetchPageData2(response.data.access_token);
      localStorage.setItem("token", response.data.access_token);
      return true;
    });
  };
  const fetchPageData = async (access_token) => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access_token}`,
    };
    setIsLoading(true);
    await axios
      .get("http://precise-algo.local.com/fetch/home", { headers })
      .then((response) => {
        setParagraphs(response.data);
        setbreadCrumb(response.data.pageTitle);
        // console.log(response.data.paragraphs);
        setIsLoading(false);
      });
  };
  return (
    <>
      <NavbarTwo />

      <PageBanner
        pageTitle={breadCrumb}
        homePageUrl="/"
        homePageText="Home"
        activePageText="About Us"
        bgImgClass="item-bg1"
      />

      <AboutContent />
      {isLoading ? (
        <div className="overlay">
          <div className="spinner">
            <ScaleLoader color={"#ff4800"} loading={isLoading} size={150} />
          </div>
        </div>
      ) : (
        <>
          {service && <Services data={service} />}

          <Team />

          {partner && <PartnerContent data={partner} />}

          {carousel && <FeedbackSlider data={carousel} />}

          {pricing && <PricingCard data={pricing} />}
        </>
      )}
      <FreeTrialForm />

      <Footer />
    </>
  );
};

export default About;
