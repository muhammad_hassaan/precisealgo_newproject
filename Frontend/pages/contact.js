import React, { useEffect, useState } from "react";
import NavbarTwo from "../components/Layouts/NavbarTwo";
import PageBanner from "../components/Common/PageBanner";
import ContactFormContent from "../components/Contact/ContactFormContent";
import Footer from "../components/Layouts/Footer";
import axios from "axios";
import ScaleLoader from "react-spinners/ScaleLoader";

const Contact = () => {
  const [isLoading, setIsLoading] = useState(true);

  const clientID = "test_client";
  const secretKey = "asdf1234";
  const userName = "api";
  const accessTokenURL = "http://precise-algo.local.com/oauth/token";

  const [paragraphs, setParagraphs] = useState([]);

  // const paragraphs = data;
  useEffect(() => {
    getToken();
  }, []);

  useEffect(() => {
    //   if(paragraphs !== undefined){
    //     // console.log('in useEffect', paragraphs['paragraphs']);
    //     const arr=paragraphs['paragraphs'];
    //     arr?.map((p)=>{
    //       const {type} = p;
    //       if(type === 'carousel_wraper'){
    //         setCarousel(p);
    //       }
    //       else if(type === 'offer_solution'){
    //         setService(p);
    //       }
    //     });
    // }
  }, []);

  const getToken = async () => {
    const data = {
      grant_type: "password",
      username: userName,
      password: secretKey,
      client_id: clientID,
    };
    const options = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      withCredentials: true,
    };
    await axios.post(accessTokenURL, data, options).then((response) => {
      // console.log(response.data.access_token);
      fetchPageData(response.data.access_token);
      localStorage.setItem("token", response.data.access_token);
      return true;
    });
  };
  const fetchPageData = async (access_token) => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access_token}`,
    };
    setIsLoading(true);
    await axios
      .get("http://precise-algo.local.com/fetch/contact", { headers })
      .then((response) => {
        // console.log(response.data);
        setParagraphs(response.data.paragraphs);
        console.log(response.data);
        setIsLoading(false);
      });
  };
  return (
    <>
      <NavbarTwo />

      <PageBanner
        pageTitle="Contact"
        homePageUrl="/"
        homePageText="Home"
        activePageText="Contact"
        bgImgClass="item-bg3"
      />
      {isLoading ? (
        <div className="overlay">
          <div className="spinner">
            <ScaleLoader color={"#ff4800"} loading={isLoading} size={150} />
          </div>
        </div>
      ) : (
        <>
          <ContactFormContent data={paragraphs} />
        </>
      )}

      <Footer />
    </>
  );
};

export default Contact;
