import React, { useEffect, useState } from "react";
import NavbarTwo from "../components/Layouts/NavbarTwo";
import PageBanner from "../components/Common/PageBanner";
import TeamContent from "../components/Team/TeamContent";
import FreeTrialForm from "../components/Common/FreeTrialForm";
import Footer from "../components/Layouts/Footer";
import axios from "axios";
import ScaleLoader from "react-spinners/ScaleLoader";

const Team = () => {
  const clientID = "test_client";
  const secretKey = "asdf1234";
  const userName = "api";
  const accessTokenURL = "http://precise-algo.local.com/oauth/token";

  const [paragraphs, setParagraphs] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [breadCrumbTitle, setbreadCrumbTitle] = useState();

  useEffect(() => {
    const access_token = localStorage.getItem("token");
    const token_expiry = localStorage.getItem("expiry");

    if (access_token == null || Date.now() > token_expiry) {
      getToken();
    } else if (Date.now() <= token_expiry - 5 * 60 * 1000) {
      getToken();
    } else {
      fetchPageData(access_token);
    }
  }, []);

  useEffect(() => {
    if (paragraphs !== undefined) {
      const arr = paragraphs["paragraphs"];
      arr?.map((p) => {
        const { type } = p;
        if (type === "carousel_wraper") {
          setCarousel(p);
        } else if (type === "offer_solution") {
          setService(p);
        }
      });
    }
  }, [paragraphs]);

  const getToken = async () => {
    const data = {
      grant_type: "password",
      username: userName,
      password: secretKey,
      client_id: clientID,
    };
    const options = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      withCredentials: true,
    };
    await axios.post(accessTokenURL, data, options).then((response) => {
      fetchPageData(response.data.access_token);
      localStorage.setItem("token", response.data.access_token);
      return true;
    });
  };
  const fetchPageData = async (access_token) => {
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access_token}`,
    };
    setIsLoading(true);
    await axios
      .get("http://precise-algo.local.com/fetch/team", { headers })
      .then((response) => {
        setParagraphs(response.data.paragraphs);
        // console.log(response.data.paragraphs[0])
        setbreadCrumbTitle(response.data.pageTitle);
        setIsLoading(false);
      });
  };

  // console.log(paragraphs);
  return (
    <>
      <NavbarTwo />

      <PageBanner
        pageTitle={breadCrumbTitle}
        homePageUrl="/"
        homePageText="Home"
        activePageText="Team"
        bgImgClass="item-bg1"
      />
      {isLoading ? (
        <div className="overlay">
          <div className="spinner">
            <ScaleLoader color={"#ff4800"} loading={isLoading} size={150} />
          </div>
        </div>
      ) : (
        <>
          {<TeamContent data={paragraphs} />}
        </>
      )}
      

      <FreeTrialForm />

      <Footer />
    </>
  );
};

export default Team;
