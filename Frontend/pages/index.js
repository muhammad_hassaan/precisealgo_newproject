import React, { useEffect, useState } from "react";
import Navbar from "../components/Layouts/Navbar";
import MainBanner from "../components/HomeOne/MainBanner";
import About from "../components/HomeOne/About";
import Services from "../components/HomeOne/Services";
import Webinar from "../components/HomeOne/Webinar";
import PartnerContent from "../components/Common/PartnerContent";
import FeedbackSlider from "../components/Common/FeedbackSlider";
import PricingCard from "../components/Common/PricingCard";
import BlogPost from "../components/Common/BlogPost";
import FreeTrialForm from "../components/Common/FreeTrialForm";
import Footer from "../components/Layouts/Footer";
import axios from "axios";
import * as process from "process";
// import data from "./node.json";
import ScaleLoader from "react-spinners/ScaleLoader";

const Index = () => {
  const clientID = "test_client";
  const secretKey = "asdf1234";
  const userName = "api";
  const accessTokenURL = "http://precise-algo.local.com/oauth/token";

  const [paragraphs, setParagraphs] = useState([]);
  const [hero, setHero] = useState();
  const [about, setAbout] = useState();
  const [service, setService] = useState();
  const [webinar, setWebinar] = useState();
  const [partner, setPartner] = useState();
  const [carousel, setCarousel] = useState();
  const [pricing, setPricing] = useState();
  const [blogpost, setBlogPost] = useState();
  const [URL, setURL] = useState(process.env.REACT_APP_BASE_PATH);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const access_token = localStorage.getItem("token");
    const token_expiry = localStorage.getItem("expiry");
    if (access_token == null) {
      // alert('test');
      getToken();
    } else if (Date.now() <= token_expiry - 5 * 60 * 1000) {
      // Token life is less than 5 minutes
      fetchPageData(access_token);
    } else {
      // Use the old token
      fetchPageData();
    }
  }, []);

  useEffect(() => {
    if (paragraphs !== undefined) {
      const arr = paragraphs["paragraphs"];
      arr?.map((p) => {
        const { type } = p;
        if (type === "hero") {
          setHero(p);
        } else if (type === "engage_audiences") {
          setAbout(p);
        } else if (type === "offer_solution") {
          setService(p);
        } else if (type === "webinar") {
          setWebinar(p);
        } else if (type === "customer_partner") {
          setPartner(p);
        } else if (type === "carousel_wraper") {
          setCarousel(p);
        } else if (type === "pricing") {
          setPricing(p);
        } else if (type === "recent_news") {
          setBlogPost(p);
        }
      });
    }
  }, [paragraphs]);

  const getToken = async () => {
    // console.log(URL);
    const data = {
      grant_type: "password",
      username: userName,
      password: secretKey,
      client_id: clientID,
    };
    const options = {
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      withCredentials: true,
    };
    await axios.post(accessTokenURL, data, options).then((response) => {
      fetchPageData(response.data.access_token);
      localStorage.setItem("token", response.data.access_token);
      return true;
    });
  };
  const fetchPageData = async () => {
    const access_token = localStorage.getItem("token");
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${access_token}`,
    };
    await axios
      .get("http://precise-algo.local.com/fetch/home", { headers })
      .then((response) => {
        setParagraphs(response.data);
        setIsLoading(false);
      });
  };

  return (
    <>
      <Navbar />
      {isLoading ? (
        <div className="overlay">
          <div className="spinner">
            <ScaleLoader color={"#ff4800"} loading={isLoading} size={150} />
          </div>
        </div>
        ) : (
        <>
          {hero && <MainBanner data={hero} />}

          {about && <About data={about} />}

          {service && <Services data={service} />}

          {webinar && <Webinar data={webinar} />}

          {partner && <PartnerContent data={partner} />}

          {carousel && <FeedbackSlider data={carousel} />}

          {pricing && <PricingCard data={pricing} />}

          {blogpost && <BlogPost data={blogpost} />}
        </>
      )}

      <FreeTrialForm />

      <Footer />
    </>
  );
};

export default Index;
