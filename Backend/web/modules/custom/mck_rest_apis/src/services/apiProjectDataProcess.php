<?php

namespace Drupal\mck_rest_apis\Services;

use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


class apiProjectDataProcess
{
  /**
   * Messenger Service Object
   */
  protected $logger;

  /**
   * Part of the DependencyInjection.
   * @param LoggerInterface $logger
   */
  public function __construct()
  {

  }

  /**
   * @param string $obj
   * @return mixed|null
   */
  public function getJSON($nid)
  {
    try {
      $node = Node::load($nid);
    //   dump($node); exit;

      $response['id'] = $node->get('uuid')->getValue()[0]['value'];
      $response['type'] = $node->getType();
      $response['customername'] = $node->field_customername->getValue()[0]['value'];
      $response['startdate'] = $node->field_start_date->getValue()[0]['value'];
      $response['enddate'] = $node->field_end_date->getValue()[0]['value'];
      $response['status'] = $node->field_status->getValue()[0]['value'];
      $response['demolink'] = $node->field_demo_link->getValue()[0]['value'];
      $response['title'] = $node->field_title->getValue()[0]['value'];
      $response['description'] = $node->field_description->getValue()[0]['value'];
      $response['descriptiontwo'] = $node->field_description_two->getValue()[0]['value'];
      $response['testimonial'] = $node->field_testimonial->getValue()[0]['value'];
      $response['descriptionthree'] = $node->field_description_three->getValue()[0]['value'];
      $response['descriptionfour'] = $node->field_description_four->getValue()[0]['value'];
      
      // dump($response); exit;
      return $response;

    } catch (Exception $e) {
    //   dump('error');
    //   exit;
      $this->logger->error("There was an error \n" . $e->getMessage());
    }
  }





}
