<?php

namespace Drupal\mck_rest_apis\Services;

use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


class apiDataProcess
{


  /**
   * Messenger Service Object
   */
  protected $logger;

  /**
   * Part of the DependencyInjection.
   * @param LoggerInterface $logger
   */
  public function __construct()
  {

  }

  /**
   * @param string $obj
   * @return mixed|null
   */
  public function getJSON($nid)
  {
    try {
      $node = Node::load($nid);

      if ($node->hasField('field_paragraph') and !empty($node->field_paragraph->getValue())) {
        $paragraphs = $node->field_paragraph->getValue();
        $paragraphs = $this->processParagraphs($paragraphs);
        $response['paragraphs'] = $paragraphs;
      }
      return $response;

    } catch (Exception $e) {
      dump('error');
      exit;

      $this->logger->error("There was an error \n" . $e->getMessage());
    }
  }

  /**
   * Process Attached Paragraphs
   * @params array
   * - $paragraphs
   */
  private function processParagraphs($paragraphs)
  {
    $output = [];
    if ($paragraphs) {
      foreach ($paragraphs as $key => $element) {
        $object = [];
        if ($element['target_id']) {
          $paragraph = Paragraph::load($element['target_id']);
//  print_r($paragraph->getType().'     ');
          if ($paragraph->getType() === 'hero') {
            $object = $this->heroParagraph($paragraph);
          } else if ($paragraph->getType() == 'engage_audiences') {
            $object = $this->engageAudience($paragraph);
          } else if ($paragraph->getType() == 'offer_solution') {
            $object = $this->offerSolution($paragraph);
          } else if ($paragraph->getType() == 'webinar') {
            $object = $this->webinar($paragraph);
          } else if ($paragraph->getType() == 'customer_partner') {
            $object = $this->customerAndPartner($paragraph);
          } else if ($paragraph->getType() == 'carousel_wraper') {
            $object = $this->carouselWrapper($paragraph);
          } else if ($paragraph->getType() == 'pricing') {
            $object = $this->pricingSection($paragraph);
          } else if ($paragraph->getType() == 'recent_news') {
            $object = $this->recentNews($paragraph);
          } else if ($paragraph->getType() == 'offer_solution_item') {
            $object = $this->messageContact($paragraph);
          } else if ($paragraph->getType() == 'contact_section') {
            $object = $this->contactSection($paragraph);
          } else if ($paragraph->getType() == 'team_section') {
            $object = $this->teamSectionParagraph($paragraph);
          } elseif ($paragraph->getType() == 'about_us_team') {
            $object = $this->about_us_team($paragraph);
          }

          $object['type'] = $paragraph->getType();

          array_push($output, $object);
        }
      }
    }
    return $output;
  }


  /**
   * Process Hero Paragraph
   */
  private function heroParagraph($paragraph)
  {
    if ($paragraph->field_hero_services->getValue()) {
      $sub_paragraph_output = [];
      foreach ($paragraph->field_hero_services->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $object_paragraph['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $object_paragraph['type'] = $sub_paragraph->getType();
        $object_paragraph['icon'] = $sub_paragraph->field_icon->getValue()[0]['value'];
        $object_paragraph['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $object_paragraph['description'] = $sub_paragraph->field_description->getValue()[0]['value'];
        array_push($sub_paragraph_output, $object_paragraph);
      }
    }
    $img_id = $paragraph->field_hero_image->getValue()[0]['target_id'];
    if ($img_id) {
      $imgUrl = \Drupal\image\Entity\ImageStyle::load('wide')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
    }
    $object = [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'hero_service' => $sub_paragraph_output,
      'image' => $imgUrl
    ];
    return $object;
  }

  /**
   * Process Engage Audiance Paragraph
   */
  private function engageAudience($paragraph)
  {
    $type = $paragraph->getType();
    if ($type == 'engage_audiences') {
      $img_id = $paragraph->field_hero_image->getValue()[0]['target_id'];
      if ($img_id) {
        $imgUrl1 = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
      }
      $img_id = $paragraph->field_hero_image->getValue()[0]['target_id'];
      if ($img_id) {
        $imgUrl2 = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
      }
      $object = [
        'id' => $paragraph->get('uuid')->getValue()[0]['value'],
        'type' => $paragraph->getType(),
        'title' => $paragraph->get('field_title')->getValue()[0]['value'],
        'description' => $paragraph->get('field_description')->getValue()[0]['value'],
        'link' => $paragraph->get('field_link')->getValue()[0]['value'],
        'image1' => $imgUrl1,
        'image2' => $imgUrl2

      ];

      return $object;
    }
  }

  /**
   * Process offerSolution Paragraph
   */
  private function offerSolution($paragraph)
  {
    $sub_paragraph_output = [];
    if ($paragraph->field_items->getValue()) {
      foreach ($paragraph->field_items->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $sub_paragraph_output[$key]['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $sub_paragraph_output[$key]['type'] = $sub_paragraph->getType();
        $sub_paragraph_output[$key]['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $sub_paragraph_output[$key]['description'] = $sub_paragraph->field_description->getValue()[0]['value'];
        $sub_paragraph_output[$key]['icon'] = $sub_paragraph->field_icon->getValue()[0]['value'];
      }
    }
    return [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'paragraphs' => $sub_paragraph_output,
    ];
  }

  /**
   * Process Webinar Paragraph
   */
  private function webinar($paragraph)
  {

    $img_id = $paragraph->field_hero_image->getValue()[0]['target_id'];
    if ($img_id) {
      $imgUrl = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
    }
    $type = $paragraph->getType();
    if ($type == 'webinar') {
      $object = [
        'id' => $paragraph->get('uuid')->getValue()[0]['value'],
        'type' => $paragraph->getType(),
        'title' => $paragraph->get('field_title')->getValue()[0]['value'],
        'description' => $paragraph->get('field_description')->getValue()[0]['value'],
        'link' => $paragraph->get('field_link')->getValue()[0]['value'],
        'image' => $imgUrl,

      ];
      return $object;
    }
  }

  /**
   * Process customer and partner Paragraph
   */
  private function customerAndPartner($paragraph)
  {
    $type = $paragraph->getType();
    // print_r($type.'   ');
    if ($type == 'customer_partner') {
      if ($paragraph->field_partner_logo_item->getValue()) {
        foreach ($paragraph->field_partner_logo_item->getValue() as $key => $ele) {
          $sub_paragraph = Paragraph::load($ele['target_id']);
          $sub_paragraph_output[$key]['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
          $sub_paragraph_output[$key]['type'] = $sub_paragraph->getType();
          $img_id = $sub_paragraph->field_image->getValue()[0]['target_id'];
          if ($img_id) {
            $sub_paragraph_output[$key]['imgUrl'] = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
          }
          $sub_paragraph_output[$key]['link'] = $sub_paragraph->field_link->getValue()[0]['uri'];
        }
      }
      $object = [
        'id' => $paragraph->get('uuid')->getValue()[0]['value'],
        'type' => $paragraph->getType(),
        'title' => $paragraph->get('field_title')->getValue()[0]['value'],
        'description' => $paragraph->get('field_description')->getValue()[0]['value'],
        'paragraphs' => $sub_paragraph_output,
      ];

      return $object;
    }
  }

  /**
   * Process Carousel Wrapper Paragraph
   */
  private function carouselWrapper($paragraph)
  {
    $sub_paragraph_output = [];
    if ($paragraph->field_carousel_item->getValue()) {
      foreach ($paragraph->field_carousel_item->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $sub_paragraph_output[$key]['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $sub_paragraph_output[$key]['type'] = $sub_paragraph->getType();
        $sub_paragraph_output[$key]['description'] = $sub_paragraph->field_description->getValue()[0]['value'];
        $sub_paragraph_output[$key]['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $sub_paragraph_output[$key]['designation'] = $sub_paragraph->field_designation->getValue()[0]['value'];
        $img_id = $sub_paragraph->field_hero_image->getValue()[0]['target_id'];
        if ($img_id) {
          $sub_paragraph_output[$key]['imgUrl'] = \Drupal\image\Entity\ImageStyle::load('carousel_image')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
        }
      }
    }
    $object = [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'paragraphs' => $sub_paragraph_output,
    ];
    return $object;
  }

  /**
   * Process Pricing Paragraph
   */

  private function pricingSection($paragraph)
  {
    $sub_paragraph_output = [];
    if ($paragraph->field_pricing_card->getValue()) {
      foreach ($paragraph->field_pricing_card->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $sub_paragraph_output[$key]['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $sub_paragraph_output[$key]['type'] = $sub_paragraph->getType();
        $sub_paragraph_output[$key]['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $sub_paragraph_output[$key]['description'] = $sub_paragraph->field_description->getValue()[0]['value'];
        $sub_paragraph_output[$key]['price'] = $sub_paragraph->field_price->getValue()[0]['value'];
        $sub_paragraph_output[$key]['price'] = $sub_paragraph->field_yearly->getValue()[0]['value'];
        $sub_paragraph_output[$key]['link'] = $sub_paragraph->field_link->getValue()[0]['value'];
        $list = [];
        $label = [];
        $list_entries = $sub_paragraph->field_price_list->getSetting('allowed_values');
        foreach ($list_entries as $item) {
          array_push($label, $item);
        }
        foreach ($sub_paragraph->field_price_list->getValue() as $item) {
          array_push($list, $item['value']);
        }
        $sub_paragraph_output[$key]['list'] = $list;
        $sub_paragraph_output[$key]['label'] = $label;
      }
    }

    return [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'paragraphs' => $sub_paragraph_output,
    ];
  }

  /**
   * Process Recent News Paragraph
   */
  private function recentNews($paragraph)
  {
    $nids = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'blog')
      ->execute();
    $sub_paragraph_output = [];

    foreach ($nids as $key => $nid) {
      $node = Node::load($nid);
      $object['id'] = $node->get('uuid')->getValue()[0]['value'];
      $object['title'] = $node->title->getValue()[0]['value'];
      if ($node->hasField('field_blog_p') and !empty($node->field_blog_p->getValue())) {
        $paragraphs = $node->field_blog_p->getValue();
        $paragraphs = $this->processBlogParagraphs($paragraphs);
        $object['description'] = $paragraphs;
      }
      $img_id = $node->field_blog_image->getValue()[0]['target_id'];
      $object['imgUri'] = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
      if ($node->hasField('field_created_on') and !empty($node->field_created_on->getValue())) {
        $object['date'] = $node->field_created_on->getValue()[0]['value'];
      }
      array_push($sub_paragraph_output, $object);
    }

    return [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'paragraphs' => $sub_paragraph_output,
    ];
  }

  /**
   * Process message Paragraph
   */
  private function messageContact($paragraph)
  {
    $object = [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'icon' => $paragraph->field_icon->getValue()[0]['value'],
    ];
    return $object;
  }

  /**
   * Process contact section Paragraph
   */
  private function contactSection($paragraph)
  {

    if ($paragraph->field_social_section->getValue()) {
      $sub_paragraph = Paragraph::load($paragraph->field_social_section->getValue()[0]['target_id']);

      $sub_paragraph_output = [];
      if ($sub_paragraph->field_social_item->getValue()) {
        foreach ($sub_paragraph->field_social_item->getValue() as $key => $element) {
          $sub_sub_paragraph = Paragraph::load($element['target_id']);
          $sub_object['id'] = $sub_sub_paragraph->get('uuid')->getValue()[0]['value'];
          $sub_object['type'] = $sub_sub_paragraph->getType();
          $sub_object['icon_classnames'] = $sub_sub_paragraph->field_icon_classnames->getValue()[0]['value'];
          $sub_object['link'] = $sub_sub_paragraph->field_link->getValue()[0]['uri'];
          array_push($sub_paragraph_output, $sub_object);
        }
      }
    }
    $img_id = $paragraph->field_hero_image->getValue()[0]['target_id'];
    if ($img_id) {
      $imgUrl = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
    }
    $object = [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'email' => $paragraph->field_email->getValue()[0]['value'],
      'contact_no' => $paragraph->field_contact_no->getValue()[0]['value'],
      'social_section' => $sub_paragraph_output,
      'imgUrl' => $imgUrl,
    ];

    return $object;
  }

  /**
   * Process Team section  Paragraph
   */
  private function teamSectionParagraph($paragraph)
  {
    $sub_paragraph_output = [];
    if ($paragraph->field_team_item->getValue()) {
      foreach ($paragraph->field_team_item->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $sub_sub_paragraph_output = [];
        if ($sub_paragraph->field_paragraph->getValue()) {
          foreach ($sub_paragraph->field_paragraph->getValue() as $key2 => $ele2) {
            $sub_sub_paragraph = Paragraph::load($ele2['target_id']);

            $sub_object['title'] = $sub_sub_paragraph->field_link->getValue()[0]['uri'];
            $sub_object['icon'] = $sub_sub_paragraph->field_icon_classnames->getValue()[0]['value'];
            array_push($sub_sub_paragraph_output, $sub_object);
          }
        }
        $object['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $object['type'] = $sub_paragraph->getType();
        $object['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $object['designation'] = $sub_paragraph->field_designation->getValue()[0]['value'];
        $object['social'] = $sub_sub_paragraph_output;
        $img_id = $sub_paragraph->field_hero_image->getValue()[0]['target_id'];
        if ($img_id) {
          $object['imgUrl'] = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
        }
        array_push($sub_paragraph_output, $object);
      }
    }
    $object1 = [
      'paragraphs' => $sub_paragraph_output,
    ];


    return $object1;

  }

  /**
   * Process Team section Paragraph on about us page
   */
  private function about_us_team($paragraph)
  {

//    this section is loaded from team page
    $nids = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'page')
      ->condition('field_page_type', 'team')
      ->execute();
    foreach ($nids as $key => $nid) {
      $target_nid = $nid;
    }
    $node = Node::load($target_nid);
    $sub_paragraph_output = [];
    if ($paragraph->field_about_team_item->getValue()) {
      foreach ($paragraph->field_about_team_item->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $sub_sub_paragraph_output = [];
        if ($sub_paragraph->field_paragraph->getValue()) {
          foreach ($sub_paragraph->field_paragraph->getValue() as $key2 => $ele2) {
            $sub_sub_paragraph = Paragraph::load($ele2['target_id']);

            $sub_object['title'] = $sub_sub_paragraph->field_link->getValue()[0]['uri'];
            $sub_object['icon'] = $sub_sub_paragraph->field_icon_classnames->getValue()[0]['value'];
            array_push($sub_sub_paragraph_output, $sub_object);
          }
        }
        $object['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $object['type'] = $sub_paragraph->getType();
        $object['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $object['designation'] = $sub_paragraph->field_designation->getValue()[0]['value'];
        $object['social'] = $sub_sub_paragraph_output;
        $img_id = $sub_paragraph->field_hero_image->getValue()[0]['target_id'];
        if ($img_id) {
          $object['imgUrl'] = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
        }
        array_push($sub_paragraph_output, $object);
      }
    }
    $object1 = [
      'title' => $paragraph->get('field_title')->getValue()[0]['value'],
      'description' => $paragraph->get('field_description')->getValue()[0]['value'],
      'paragraphs' => $sub_paragraph_output,
    ];
    // if ($node->hasField('field_paragraph') and !empty($node->field_paragraph->getValue())) {
    //   $team_paragraphs = $node->field_paragraph->getValue();
    //   $team_paragraphs = $this->processParagraphs($team_paragraphs);
    //   $sub_paragraph_output = $team_paragraphs;
    // }
    // $img_id = $paragraph->field_hero_image->getValue()[0]['target_id'];
    // if ($img_id) {
    //   $imgUrl = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
    // }
    // $object = [
    //   'id' => $paragraph->get('uuid')->getValue()[0]['value'],
    //   'type' => $paragraph->getType(),
    //   'title' => $paragraph->get('field_title')->getValue()[0]['value'],
    //   'description' => $paragraph->get('field_description')->getValue()[0]['value'],
    //   'paragraphs' => $sub_paragraph_output,
    //   'imgUrl' => $imgUrl,
    // ];

    return $object1;
  }
  private function processBlogParagraphs($paragraphs)
  {

    $output = [];
    if ($paragraphs) {
      foreach ($paragraphs as $key => $element) {
        $object = [];
        if ($element['target_id']) {
          $paragraph = Paragraph::load($element['target_id']);
          $object['title'] = $paragraph->get('field_title')->getValue()[0]['value'];
          $object['description'] = $paragraph->get('field_description')->getValue()[0]['value'];
          array_push($output, $object);
        }
      }
    }
    return $output[0]['description'];
  }


}
