<?php

namespace Drupal\mck_rest_apis\Services;

use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


class apiBlogDataProcess
{


  /**
   * Messenger Service Object
   */
  protected $logger;

  /**
   * Part of the DependencyInjection.
   * @param LoggerInterface $logger
   */
  public function __construct()
  {

  }

  /**
   * @param string $obj
   * @return mixed|null
   */
  public function getJSON($nid)
  {
    try {
      $node = Node::load($nid);

      if ($node->hasField('field_blog_p') and !empty($node->field_blog_p->getValue())) {
        $paragraphs = $node->field_blog_p->getValue();
        $paragraphs = $this->processParagraphs($paragraphs);
        $response['paragraphs'] = $paragraphs;
      }
      if ($node->hasField('field_blog_image') and !empty($node->field_blog_image->getValue())) {
        $images = $node->field_blog_image->getValue();
        $images = $this->processImages($images);
        $response['images'] = $images;
      }
      if ($node->hasField('field_category') and !empty($node->field_category->getValue())) {
        $categories = $node->field_category->getValue();
        $categories = $this->processTaxonomyTerms($categories);
        $response['categories'] = $categories;
      }
      if ($node->hasField('field_tags') and !empty($node->field_tags->getValue())) {
        $tags = $node->field_tags->getValue();
        $tags = $this->processTaxonomyTerms($tags);
        $response['tags'] = $tags;
      }
      if ($node->hasField('field_created_on') and !empty($node->field_created_on->getValue())) {
        $response['date'] = $node->field_created_on->getValue()[0]['value'];
      }
      return $response;

    } catch (Exception $e) {

      $this->logger->error("There was an error \n" . $e->getMessage());
    }
  }

  /**
   * Process Attached Paragraphs
   * @params array
   * - $paragraphs
   */
  private function processParagraphs($paragraphs)
  {

    $output = [];
    if ($paragraphs) {
      foreach ($paragraphs as $key => $element) {
        $object = [];
        if ($element['target_id']) {
          $paragraph = Paragraph::load($element['target_id']);
          $object['title'] = $paragraph->get('field_title')->getValue()[0]['value'];
          $object['description'] = $paragraph->get('field_description')->getValue()[0]['value'];
          array_push($output, $object);
        }
      }
    }
    return $output;
  }

  /**
   * Process Attached Images
   * @params array
   * - $Images
   */
  private function processImages($images)
  {

    $output = [];
    if ($images) {
      foreach ($images as $key => $element) {
        $object = [];
        if ($element['target_id']) {
          $object['imgUrl'] = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($element['target_id'])->getFileUri());
          array_push($output, $object);
        }
      }
    }
    return $output;
  }

  /**
   * Process Attached Images
   * @params array
   * - $Images
   */
  private function processTaxonomyTerms($terms)
  {

    $output = [];
    if ($terms) {
      foreach ($terms as $key => $element) {
        $object = [];
        if ($element['target_id']) {
          $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($element['target_id']);
          $object = $term->name->value;
          array_push($output, $object);
        }
      }
    }
    return $output;
  }
}
