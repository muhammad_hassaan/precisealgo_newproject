<?php


/**
 * The following custom rest api will be used to return values of a node
 */

namespace Drupal\mck_rest_apis\Plugin\rest\resource;

use Drupal;
use Drupal\Component\Utility\Image;
use Drupal\Core\Image\Image as ImageImage;
use Drupal\Core\Render\Element\File as ElementFile;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;

// require_once Drupal::root() . '/sites/default/files/my_file.jpg';


// use Laminas\Diactoros\Response\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Represents entities as resources.*
 * @see \Drupal\rest\Plugin\Deriver\EntityDeriver
 *
 * @RestResource(
 *   id = "fetch_any_specific_blog",
 *   label = @Translation("Any blog given in params"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/fetch/blog/{blog}"
 *   }
 * )
 */
class FetchBlogDetails extends ResourceBase
{
  /**
   * A current user instance.
   *
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param LoggerInterface $logger
   *   A logger instance.
   * @param AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array                 $configuration,
                          $plugin_id,
                          $plugin_definition,
    array                 $serializer_formats,
    LoggerInterface       $logger,
    AccountProxyInterface $current_user
  )
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user')
    );
  }


  public function get($blog = null)
  {

    $blog_object = [];

    if ($blog == 'all') {
      $nids = \Drupal::entityQuery('node')
        ->accessCheck(TRUE)
        ->condition('type', 'blog')
        ->execute();
      foreach ($nids as $key => $nid) {
        $node = Node::load($nid);
        $object['title'] = $node->title->getValue()[0]['value'];
        if ($node->hasField('field_blog_p') and !empty($node->field_blog_p->getValue())) {
          $paragraphs = $node->field_blog_p->getValue();
          $paragraphs = $this->processParagraphs($paragraphs);
          $object['description'] = $paragraphs;
        }
        $img_id = $node->field_blog_image->getValue()[0]['target_id'];
        $object['imgUri'] = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
        if ($node->hasField('field_created_on') and !empty($node->field_created_on->getValue())) {
          $object['date'] = $node->field_created_on->getValue()[0]['value'];
        }
        array_push($blog_object, $object);
      }
      return (new JsonResponse($blog_object));
    }

    $blog = str_replace("-", " ", $blog);
    $nids = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'blog')
      ->condition('title', $blog)
      ->execute();
    foreach ($nids as $key => $nid) {
      $target_nid = $nid;
    }
    $jsonObject = \Drupal::service('mck_rest_apis.API_blog_data_process')->getJSON($target_nid);
//    dump($jsonObject);
//    exit;

    return (new JsonResponse($jsonObject));
  }

  /**
   * Process Attached Paragraphs
   * @params array
   * - $paragraphs
   */
  private function processParagraphs($paragraphs)
  {

    $output = [];
    if ($paragraphs) {
      foreach ($paragraphs as $key => $element) {
        $object = [];
        if ($element['target_id']) {
          $paragraph = Paragraph::load($element['target_id']);
          $object['title'] = $paragraph->get('field_title')->getValue()[0]['value'];
          $object['description'] = $paragraph->get('field_description')->getValue()[0]['value'];
          array_push($output, $object);
        }
      }
    }
    return $output[0]['description'];
  }
}

