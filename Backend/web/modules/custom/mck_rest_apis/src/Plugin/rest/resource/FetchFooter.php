<?php

/**
 * The following custom rest api will be used to return values of a node
 */

namespace Drupal\mck_rest_apis\Plugin\rest\resource;

use Drupal;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Exception;

// use Laminas\Diactoros\Response\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Represents entities as resources.*
 * @see \Drupal\rest\Plugin\Deriver\EntityDeriver
 *
 * @RestResource(
 *   id = "fetch_footer_contents",
 *   label = @Translation("Footer Content"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/fetch/footer"
 *   }
 * )
 */
class FetchFooter extends ResourceBase
{
  /**
   * A current user instance.
   *
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param LoggerInterface $logger
   *   A logger instance.
   * @param AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array                 $configuration,
    $plugin_id,
    $plugin_definition,
    array                 $serializer_formats,
    LoggerInterface       $logger,
    AccountProxyInterface $current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user')
    );
  }

  public function get()
  {
    $menu_items = [];
    $menu_links = \Drupal::menuTree()->load('main', new \Drupal\Core\Menu\MenuTreeParameters());
    // Loop through the main menu links
    foreach ($menu_links as $menu_link) {
      $expanded = false;
      $sub_tree = $menu_link->subtree;
      if (!empty($sub_tree)) {
        $sub_menu_items = [];
        $expanded = true;
        foreach ($sub_tree as $sub_tree_item) {
          $submenu_links = [
            'id' => $sub_tree_item->link->getTitle(),
            'title' => $sub_tree_item->link->getTitle(),
            'url' => $sub_tree_item->link->getUrlObject()->toString(),
            'submenu' => [],
          ];
          array_push($sub_menu_items, $submenu_links);
        }
      }
      $object = [
        'id' => $menu_link->link->getTitle(),
        'title' => $menu_link->link->getTitle(),
        'url' => $menu_link->link->getUrlObject()->toString(),
        'submenu' => $sub_menu_items,
        'expanded' => $expanded
      ];
      $response['menu_items'] = $menu_items;

      array_push($menu_items, $object);
    }
    //    ///////////////////////services section/////////////////
    $nids = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'page')
      ->condition('field_page_type', 'services')
      ->execute();
    foreach ($nids as $key => $nid) {
      $target_nid = $nid;
    }
    $node = Node::load($target_nid);
    if ($node->hasField('field_paragraph') and !empty($node->field_paragraph->getValue())) {
      $paragraphs = $node->field_paragraph->getValue();
      $paragraphs = $this->processParagraphs($paragraphs);
      $response['services'] = $paragraphs;
    }
       //    ///////////////////////Contact section/////////////////
       $nids = \Drupal::entityQuery('node')
       ->accessCheck(TRUE)
       ->condition('type', 'page')
       ->condition('field_page_type', 'contact')
       ->execute();
     foreach ($nids as $key => $nid) {
       $target_nid = $nid;
     }
     $node = Node::load($target_nid);
    //  dump($node); exit;
     if ($node->hasField('field_paragraph') and !empty($node->field_paragraph->getValue())) {
       $paragraphs = $node->field_paragraph->getValue();
       $paragraphs = $this->processParagraphs($paragraphs);
       $response['contact'] = $paragraphs;
     }
    return (new JsonResponse($response));
  }

  /**
   * Process Attached Paragraphs
   * @params array
   * - $paragraphs
   */
  private function processParagraphs($paragraphs)
  {
    $output = [];
    if ($paragraphs) {
      foreach ($paragraphs as $key => $element) {

        if ($element['target_id']) {
          $paragraph = Paragraph::load($element['target_id']);
          // print_r($paragraph->getType() . '   ');
          if ($paragraph->getType() == 'offer_solution') {
            $object = $this->offerSolution($paragraph);
          }
          else if($paragraph->getType() == 'contact_section'){
            $object = $this->contactSection($paragraph);
          }
        }
      }
    }
    return $object;
  }

  /**
   * Process offerSolution Paragraph
   */
  private function offerSolution($paragraph)
  {
    $sub_paragraph_output = [];
    if ($paragraph->field_items->getValue()) {
      foreach ($paragraph->field_items->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $object['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $object['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        array_push($sub_paragraph_output, $object);
      }
    }
    // dump($sub_paragraph_output); exit;
    return $sub_paragraph_output;
  }

    /**
   * Process ContactSection Paragraph
   */
  private function contactSection($paragraph)
  { 
    // dump($paragraph); exit;
    $sub_paragraph_output = [];
    // if ($paragraph->field_items->getValue()) {
    //   foreach ($paragraph->field_items->getValue() as $key => $ele) {
    //     $sub_paragraph = Paragraph::load($ele['target_id']);
    //     $object['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
    //     $object['address'] = $sub_paragraph->field_address->getValue()[0]['value'];
    //     $object['email'] = $sub_paragraph->field_email->getValue()[0]['value'];
    //     $object['phone'] = $sub_paragraph->field_phone->getValue()[0]['value'];
    //     $object['fax'] = $sub_paragraph->field_fax->getValue()[0]['value'];
    //     array_push($sub_paragraph_output, $object);
    //   }
    // }
    $object = [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'address' => $paragraph->field_address->getValue()[0]['value'],
      'email' => $paragraph->field_email->getValue()[0]['value'],
      'phone' => $paragraph->field_phone->getValue()[0]['value'],
      'fax' => $paragraph->field_fax->getValue()[0]['value'],
    ];
    return $object;
  }
}
