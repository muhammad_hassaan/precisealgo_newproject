<?php


/**
 * The following custom rest api will be used to return values of a node
 */

namespace Drupal\mck_rest_apis\Plugin\rest\resource;

use Drupal;
use Drupal\Component\Utility\Image;
use Drupal\Core\Image\Image as ImageImage;
use Drupal\Core\Render\Element\File as ElementFile;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
// require_once Drupal::root() . '/sites/default/files/my_file.jpg';


// use Laminas\Diactoros\Response\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Represents entities as resources.*
 * @see \Drupal\rest\Plugin\Deriver\EntityDeriver
 *
 * @RestResource(
 *   id = "fetch_any_specific_project",
 *   label = @Translation("Any project given in params"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/fetch/project/{project}"
 *   }
 * )
 */
class FetchProjectDetails extends ResourceBase
{
  /**
   * A current user instance.
   *
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param LoggerInterface $logger
   *   A logger instance.
   * @param AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array                 $configuration,
                          $plugin_id,
                          $plugin_definition,
    array                 $serializer_formats,
    LoggerInterface       $logger,
    AccountProxyInterface $current_user
  )
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user')
    );
  }


  public function get($project)
  {
    if($project == 'all'){
      $nids = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'project')
      ->execute();
      $projects_object= [];
      foreach($nids as $key => $nid){
        $node = Node::load($nid);
        $object['title'] = $node->field_title->getValue()[0]['value'];
        $img_id = $node->field_image->getValue()[0]['target_id'];
        $object['imgUri'] = \Drupal\image\Entity\ImageStyle::load('large')->buildUrl(\Drupal\file\Entity\File::load($img_id)->getFileUri());
        array_push($projects_object, $object);
        // dump($projects_object); exit;
      }
      return (new JsonResponse($projects_object));
    }

    $project =str_replace("-"," ", $project);
    $nids = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'project')
      ->condition('field_title', $project)
      ->execute();
    foreach($nids as $key => $nid){
      $target_nid = $nid;
    }



    $jsonObject = \Drupal::service('mck_rest_apis.API_project_data_process')->getJSON($target_nid);
//    dump($jsonObject);
//    exit;

    return (new JsonResponse($jsonObject));
  }
}

