<?php

/**
 * The following custom rest api will be used to return values of a node
 */

namespace Drupal\mck_rest_apis\Plugin\rest\resource;

use Drupal;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;

// use Laminas\Diactoros\Response\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Represents entities as resources.*
 * @see \Drupal\rest\Plugin\Deriver\EntityDeriver
 *
 * @RestResource(
 *   id = "fetch_contact_info",
 *   label = @Translation("Contact Page Details"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/fetch/contact"
 *   }
 * )
 */
class FetchContact extends ResourceBase
{
  /**
   * A current user instance.
   *
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param LoggerInterface $logger
   *   A logger instance.
   * @param AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array                 $configuration,
                          $plugin_id,
                          $plugin_definition,
    array                 $serializer_formats,
    LoggerInterface       $logger,
    AccountProxyInterface $current_user
  )
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user')
    );
  }

  public function get()
  {


    $node = Node::load(7);
    if ($node->hasField('field_paragraph') and !empty($node->field_paragraph->getValue())) {
      $paragraphs = $node->field_paragraph->getValue();
      // dump($paragraphs); exit;
      $paragraphs = $this->processParagraphs($paragraphs);
      $response['paragraphs'] = $paragraphs;
      $response['pageTitle'] = $node->title->getValue()[0]['value'];
    }
    return (new JsonResponse($response));
  }

  /**
   * Process Attached Paragraphs
   * @params array
   * - $paragraphs
   */
  private function processParagraphs($paragraphs)
  {
    $output = [];
    if ($paragraphs) {
      foreach ($paragraphs as $key => $element) {
        $object = [];
        if ($element['target_id']) {
          $paragraph = Paragraph::load($element['target_id']);
          print_r($paragraph->getType() . '   ');
          if ($paragraph->getType() == 'offer_solution_item') {
            $object = $this->messageContact($paragraph);
          } else if ($paragraph->getType() == 'contact_section') {
            // dump($paragraph);exit;
            $object = $this->contactSection($paragraph);
          }
          $object['type'] = $paragraph->getType();

          array_push($output, $object);
        }
      }
    }
    return $output;
  }

  /**
   * Process message Paragraph
   */
  private function messageContact($paragraph)
  {
    $object = [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'icon' => $paragraph->field_icon->getValue()[0]['value'],
    ];
    return $object;
  }

  /**
   * Process contact section Paragraph
   */
  private function contactSection($paragraph)
  {

    if ($paragraph->field_social_section->getValue()) {
      $sub_paragraph = Paragraph::load($paragraph->field_social_section->getValue()[0]['target_id']);

      $sub_paragraph_output = [];
      if ($sub_paragraph->field_social_item->getValue()) {
        foreach ($sub_paragraph->field_social_item->getValue() as $key => $element) {
          $sub_sub_paragraph = Paragraph::load($element['target_id']);
          $sub_object['id'] = $sub_sub_paragraph->get('uuid')->getValue()[0]['value'];
          $sub_object['type'] = $sub_sub_paragraph->getType();
          $sub_object['icon_classnames'] = $sub_sub_paragraph->field_icon_classnames->getValue()[0]['value'];
          $sub_object['link'] = $sub_sub_paragraph->field_link->getValue()[0]['uri'];
          array_push($sub_paragraph_output, $sub_object);
        }
      }
    }
    $object = [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'email' => $paragraph->field_email->getValue()[0]['value'],
      'contact_no' => $paragraph->field_contact_no->getValue()[0]['value'],
      'social_section' => $sub_paragraph_output,
    ];
    // dump($object); exit;
    return $object;
  }

}
