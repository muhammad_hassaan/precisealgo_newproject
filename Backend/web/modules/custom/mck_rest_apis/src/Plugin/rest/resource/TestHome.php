<?php

/**
 * The following custom rest api will be used to return values of a node
 */

namespace Drupal\mck_rest_apis\Plugin\rest\resource;

use Drupal;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
// use Laminas\Diactoros\Response\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Represents entities as resources.*
 * @see \Drupal\rest\Plugin\Deriver\EntityDeriver
 *
 * @RestResource(
 *   id = "fetch_home",
 *   label = @Translation("Home page Details"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/fetch/test"
 *   }
 * )
 */
class TestHome extends ResourceBase
{
  /**
   * A current user instance.
   *
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param LoggerInterface $logger
   *   A logger instance.
   * @param AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array                 $configuration,
                          $plugin_id,
                          $plugin_definition,
    array                 $serializer_formats,
    LoggerInterface       $logger,
    AccountProxyInterface $current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user')
    );
  }

  public function get()
  {


    $node = Node::load(13);
    if ($node->hasField('field_paragraph') and !empty($node->field_paragraph->getValue())) {
      $paragraphs = $node->field_paragraph->getValue();
      // dump($paragraphs); exit;
      $paragraphs = $this->processParagraphs($paragraphs);
      $response['paragraphs'] = $paragraphs;
    }
    return (new JsonResponse($response));
  }

  /**
   * Process Attached Paragraphs
   * @params array
   * - $paragraphs
   */
  private function processParagraphs($paragraphs)
  {
    $output = [];
    if ($paragraphs) {
      foreach ($paragraphs as $key => $element) {
        $object = [];
        if ($element['target_id']) {
          $paragraph = Paragraph::load($element['target_id']);
          // print_r($paragraph->getType() . '   ');
          if ($paragraph->getType() === 'hero') {
            $object = $this->heroParagraph($paragraph);
          } else if ($paragraph->getType() == 'engage_audiences') {
            $object = $this->engageAudience($paragraph);
          } else if ($paragraph->getType() == 'offer_solution') {
            $object = $this->offerSolution($paragraph);
          } else if ($paragraph->getType() == 'webinar') {
            $object = $this->webinar($paragraph);
          } else if ($paragraph->getType() == 'customer_partner') {
            $object = $this->customerAndPartner($paragraph);
          } else if ($paragraph->getType() == 'carousel_wraper') {
            $object = $this->carouselWrapper($paragraph);
          } else if ($paragraph->getType() == 'pricing') {
            $object = $this->prcingSection($paragraph);
          } else if ($paragraph->getType() == 'recent_news') {
            $object = $this->recentNews($paragraph);
          }

          $object['type'] = $paragraph->getType();

          array_push($output, $object);
        }
      }
    }
    return $output;
  }

  /**
   * Process Hero Paragraph
   */
  private function heroParagraph($paragraph)
  {
    // dump($paragraph); exit;
    // $file = !empty($paragraph->field_media->getValue()) ? File::load($paragraph->field_media->getValue()[0]['target_id']) : null;
    // $fileUrl = !empty($file) ? file_create_url($file->getFileUri()) : null;
    if ($paragraph->field_hero_services->getValue()) {
      $sub_paragraph_output = [];
      foreach ($paragraph->field_hero_services->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $object_paragraph['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $object_paragraph['type'] = $sub_paragraph->getType();
        $object_paragraph['icon'] = $sub_paragraph->field_icon->getValue()[0]['value'];
        $object_paragraph['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $object_paragraph['description'] = $sub_paragraph->field_description->getValue()[0]['value'];
        array_push($sub_paragraph_output, $object_paragraph);
      }
    }
    // dump($sub_paragraph_output); exit;
    return [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'hero_service' => $sub_paragraph_output,
      // "image" => [
      //     "src" => "https://mck.hexadecimal.design/matter-governance/assets/media/img/pic-cyber-maturity-diagnostic.png",
      //     "alt" => "Cloud Security Diagnostic",
      //     "width" => "1518",
      //     "heigh" => "1018"
      // ],
    ];
  }
  /**
   * Process Engage Audiance Paragraph
   */
  private function engageAudience($paragraph)
  {
    // dump($paragraph); exit;
    $type = $paragraph->getType();
    // print_r($type.'   ');

    if ($type == 'engage_audiences') {
      return [
        'id' => $paragraph->get('uuid')->getValue()[0]['value'],
        'type' => $paragraph->getType(),
        'title' => $paragraph->get('field_title')->getValue()[0]['value'],
        'description' => $paragraph->get('field_description')->getValue()[0]['value'],
        'link' => $paragraph->get('field_link')->getValue()[0]['value'],
      ];
    }
  }

  /**
   * Process offerSolution Paragraph
   */
  private function offerSolution($paragraph)
  {
    $sub_paragraph_output = [];
    if ($paragraph->field_items->getValue()) {
      foreach ($paragraph->field_items->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $sub_paragraph_output[$key]['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $sub_paragraph_output[$key]['type'] = $sub_paragraph->getType();
        $sub_paragraph_output[$key]['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $sub_paragraph_output[$key]['description'] = $sub_paragraph->field_description->getValue()[0]['value'];
        $sub_paragraph_output[$key]['icon'] = $sub_paragraph->field_icon->getValue()[0]['value'];
      }
    }
    // dump($sub_paragraph_output); exit;
    return [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'paragraphs' => $sub_paragraph_output,
    ];
  }
  /**
   * Process Webinar Paragraph
   */
  private function webinar($paragraph)
  {
    $type = $paragraph->getType();
    // print_r($type.'   ');
    if ($type == 'webinar') {
      return [
        'id' => $paragraph->get('uuid')->getValue()[0]['value'],
        'type' => $paragraph->getType(),
        'title' => $paragraph->get('field_title')->getValue()[0]['value'],
        'description' => $paragraph->get('field_description')->getValue()[0]['value'],
        'link' => $paragraph->get('field_link')->getValue()[0]['value'],
        // 'paragraphs' => $sub_paragraph_output,
      ];
    }
  }

  /**
   * Process customer and partner Paragraph
   */
  private function customerAndPartner($paragraph)
  {
    $type = $paragraph->getType();
    // print_r($type.'   ');
    if ($type == 'customer_partner') {
      return [
        'id' => $paragraph->get('uuid')->getValue()[0]['value'],
        'type' => $paragraph->getType(),
        'title' => $paragraph->get('field_title')->getValue()[0]['value'],
        'description' => $paragraph->get('field_description')->getValue()[0]['value'],
      ];
    }
  }

  /**
   * Process Carousel Wrapper Paragraph
   */
  private function carouselWrapper($paragraph)
  {
    // dump($paragraph); exit;
    $sub_paragraph_output = [];
    if ($paragraph->field_carousel_item->getValue()) {
      foreach ($paragraph->field_carousel_item->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $sub_paragraph_output[$key]['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $sub_paragraph_output[$key]['type'] = $sub_paragraph->getType();
        $sub_paragraph_output[$key]['description'] = $sub_paragraph->field_description->getValue()[0]['value'];
        $sub_paragraph_output[$key]['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $sub_paragraph_output[$key]['designation'] = $sub_paragraph->field_designation->getValue()[0]['value'];
      }
    }
    // dump($sub_paragraph_output); exit;
    return [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'paragraphs' => $sub_paragraph_output,
    ];
  }

  /*
   * Process Pricing Paragraph
   */

  private function prcingSection($paragraph)
  {
    // dump($paragraph); exit;
    $sub_paragraph_output = [];
    if ($paragraph->field_pricing_card->getValue()) {
      foreach ($paragraph->field_pricing_card->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);


        // dump($label); exit;
        $sub_paragraph_output[$key]['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $sub_paragraph_output[$key]['type'] = $sub_paragraph->getType();
        $sub_paragraph_output[$key]['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $sub_paragraph_output[$key]['description'] = $sub_paragraph->field_description->getValue()[0]['value'];
        $sub_paragraph_output[$key]['price'] = $sub_paragraph->field_price->getValue()[0]['value'];
        $sub_paragraph_output[$key]['price'] = $sub_paragraph->field_yearly->getValue()[0]['value'];
        $sub_paragraph_output[$key]['link'] = $sub_paragraph->field_link->getValue()[0]['value'];
        // dump($sub_paragraph); exit;
        $list = [];
        $label = [];
        $list_entries = $sub_paragraph->field_price_list->getSetting('allowed_values');
        foreach($list_entries as $item){
          array_push($label, $item);
        }
        // $label_array = json_decode(json_encode($label), true);


        foreach ($sub_paragraph->field_price_list->getValue() as $item) {
          // dump($item); exit;
          array_push($list, $item['value']);
        }
        $sub_paragraph_output[$key]['list'] = $list;
        $sub_paragraph_output[$key]['label'] = $label;
        // dump($sub_paragraph_output); exit;
      }
    }

    return [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'paragraphs' => $sub_paragraph_output,
    ];
  }
  /*
   * Process Recent News Paragraph
   */
  private function recentNews($paragraph)
  {

    $sub_paragraph_output = [];
    if ($paragraph->field_news_card->getValue()) {
      foreach ($paragraph->field_news_card->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $sub_paragraph_output[$key]['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $sub_paragraph_output[$key]['type'] = $sub_paragraph->getType();
        $sub_paragraph_output[$key]['date'] = $sub_paragraph->field_news_date->getValue()[0]['value'];
        $sub_paragraph_output[$key]['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $sub_paragraph_output[$key]['description'] = $sub_paragraph->field_description->getValue()[0]['value'];
        $sub_paragraph_output[$key]['link'] = $sub_paragraph->field_link->getValue()[0]['value'];
      }
    }
    // dump($sub_paragraph_output); exit;

    return [
      'id' => $paragraph->get('uuid')->getValue()[0]['value'],
      'title' => $paragraph->field_title->getValue()[0]['value'],
      'description' => $paragraph->field_description->getValue()[0]['value'],
      'paragraphs' => $sub_paragraph_output,
    ];
  }
}
