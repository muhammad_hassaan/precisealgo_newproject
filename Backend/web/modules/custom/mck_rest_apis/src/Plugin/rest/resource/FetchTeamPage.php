<?php

/**
 * The following custom rest api will be used to return values of a node
 */

namespace Drupal\mck_rest_apis\Plugin\rest\resource;

use Drupal;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
// use Laminas\Diactoros\Response\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Represents entities as resources.*
 * @see \Drupal\rest\Plugin\Deriver\EntityDeriver
 *
 * @RestResource(
 *   id = "fetch_team_page_info",
 *   label = @Translation("Team Page Details"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/fetch/team"
 *   }
 * )
 */
class FetchTeamPage extends ResourceBase
{
  /**
   * A current user instance.
   *
   * @var AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param LoggerInterface $logger
   *   A logger instance.
   * @param AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array                 $configuration,
                          $plugin_id,
                          $plugin_definition,
    array                 $serializer_formats,
    LoggerInterface       $logger,
    AccountProxyInterface $current_user
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user')
    );
  }

  public function get()
  {


    $node = Node::load(15);
    if ($node->hasField('field_paragraph') and !empty($node->field_paragraph->getValue())) {
      $paragraphs = $node->field_paragraph->getValue();

//       dump($paragraphs); exit;
      $paragraphs = $this->processParagraphs($paragraphs);
      $response['paragraphs'] = $paragraphs;
      $response['pageTitle'] = $node->title->getValue()[0]['value'];
    }
    return (new JsonResponse($response));
  }

  /**
   * Process Attached Paragraphs
   * @params array
   * - $paragraphs
   */
  private function processParagraphs($paragraphs)
  {
    $output = [];
    if ($paragraphs) {
      foreach ($paragraphs as $key => $element) {
        $object = [];
        if ($element['target_id']) {
          $paragraph = Paragraph::load($element['target_id']);
          if ($paragraph->getType() == 'team_section') {
            $object = $this->teamSectionParagraph($paragraph);
          }
          $object['type'] = $paragraph->getType();

          array_push($output, $object);
        }
      }
    }
    return $output;
  }

  /**
   * Process Team section  Paragraph
   */
  private function teamSectionParagraph($paragraph)
  {

    $sub_paragraph_output = [];
    if ($paragraph->field_team_item->getValue()) {
      foreach ($paragraph->field_team_item->getValue() as $key => $ele) {
        $sub_paragraph = Paragraph::load($ele['target_id']);
        $sub_sub_paragraph_output = [];
        if ($sub_paragraph->field_paragraph->getValue()) {
          foreach ($sub_paragraph->field_paragraph->getValue() as $key2 => $ele2) {
            $sub_sub_paragraph = Paragraph::load($ele2['target_id']);
            $sub_object['title'] = $sub_sub_paragraph->field_link->getValue()[0]['uri'];
            $sub_object['icon'] = $sub_sub_paragraph->field_icon_classnames->getValue()[0]['value'];
            array_push($sub_sub_paragraph_output, $sub_object);
          }
        }

        $object['id'] = $sub_paragraph->get('uuid')->getValue()[0]['value'];
        $object['type'] = $sub_paragraph->getType();
        $object['title'] = $sub_paragraph->field_title->getValue()[0]['value'];
        $object['designation'] = $sub_paragraph->field_designation->getValue()[0]['value'];
        $object['social'] = $sub_sub_paragraph_output;
        array_push($sub_paragraph_output,$object);
      }
    }
    $object1 = [
      'paragraphs' => $sub_paragraph_output,
    ];
     return $object1;

  }


}
