<?php

/**
 * The following custom rest api will be used to return values of a node
 */

namespace Drupal\mck_rest_apis\Plugin\rest\resource;

use Drupal;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Exception;
// use Laminas\Diactoros\Response\JsonResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Represents entities as resources.*
 * @see \Drupal\rest\Plugin\Deriver\EntityDeriver
 *
 * @RestResource(
 *   id = "fetch_menu_links",
 *   label = @Translation("Menu links"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/fetch/menu"
 *   }
 * )
 */
class FetchMenu extends ResourceBase
{
    /**
     * A current user instance.
     *
     * @var AccountProxyInterface
     */
    protected $currentUser;

    /**
     * Constructs a Drupal\rest\Plugin\ResourceBase object.
     *
     * @param array $configuration
     *   A configuration array containing information about the plugin instance.
     * @param string $plugin_id
     *   The plugin_id for the plugin instance.
     * @param mixed $plugin_definition
     *   The plugin implementation definition.
     * @param array $serializer_formats
     *   The available serialization formats.
     * @param LoggerInterface $logger
     *   A logger instance.
     * @param AccountProxyInterface $current_user
     *   A current user instance.
     */
    public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        AccountProxyInterface $current_user
    ) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
        $this->currentUser = $current_user;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static(
            $configuration,
            $plugin_id,
            $plugin_definition,
            $container->getParameter('serializer.formats'),
            $container->get('logger.factory')->get('custom_rest'),
            $container->get('current_user')
        );
    }
    public function get()
    {
        $menu_items = [];
        $menu_links = \Drupal::menuTree()->load('main', new \Drupal\Core\Menu\MenuTreeParameters());
        // Loop through the main menu links
        foreach ($menu_links as $menu_link) {
            $expanded = false;
            $sub_tree = $menu_link->subtree;
            if (!empty($sub_tree)) {
                $sub_menu_items = [];
                $expanded = true;
                foreach ($sub_tree as $sub_tree_item) {
                    $submenu_links = [
                        'id' => $sub_tree_item->link->getTitle(),
                        'title' => $sub_tree_item->link->getTitle(),
                        'url' => $sub_tree_item->link->getUrlObject()->toString(),
                        'submenu' => [],
                    ];
                    array_push($sub_menu_items, $submenu_links);
                }
            }
            $object = [
                'id' => $menu_link->link->getTitle(),
                'title' => $menu_link->link->getTitle(),
                'url' => $menu_link->link->getUrlObject()->toString(),
                'submenu' => $sub_menu_items,
                'expanded' => $expanded
            ];

            array_push($menu_items, $object);
        }

        $response['menu_items'] = $menu_items;
        return (new JsonResponse($response));
    }
}
