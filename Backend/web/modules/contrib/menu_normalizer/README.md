# Menu Normalizer

The missing Normalizer for
[MenuLinkInterface](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Menu!MenuLinkInterface.php/interface/MenuLinkInterface/10.1.x)
and
[MenuLinkTreeElement](https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Menu!MenuLinkTreeElement.php/class/MenuLinkTreeElement/10.1.x).

## Introduction

Menu Normalizer provides normalizers for various menu objects that are missing
from Drupal core. This allows these menu objects to be (de)serialized with the
[Serialization](https://www.drupal.org/documentation/modules/serialization)
module.

## Installation

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/documentation/install/modules-themes/modules-8
for further information.

## Usage

This module does nothing on its own. Do not install it unless another module
depends on it.

## MAINTAINERS

Current maintainers:

* [Christopher C. Wells (wells)](https://www.drupal.org/u/wells)

Past maintainers:

* David Barratt ([davidwbarratt](https://www.drupal.org/u/davidwbarratt))

Current sponsors:

* [Cascade Public Media](https://www.drupal.org/cascade-public-media)

Past sponsors:

* [Golf Channel](https://www.drupal.org/node/2374873)
